package com.ridiculous.ridiculous;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Tom on 6/20/14.
 */
public class SoundsActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sounds);

        //Generate list View from ArrayList
        displayListView();

        //checkButtonClick();
    }

    private void displayListView() {
        ArrayList<SoundData> soundList = new SoundList().getArrayList();

        //create an ArrayAdaptar from the String Array
        SoundAdapter dataAdapter = new SoundAdapter(this, R.layout.sound_listitem, soundList);
        ListView listView = (ListView) findViewById(R.id.listView1);
        // Assign adapter to ListView
        listView.setAdapter(dataAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView <?> parent, View view, int position, long id) {

            }
        });

    }


    private class SoundAdapter extends ArrayAdapter<SoundData> {

        private ArrayList<SoundData> soundList;

        public SoundAdapter(Context context, int textViewResourceId, ArrayList<SoundData> soundList) {
            super(context, textViewResourceId, soundList);

            // TB TODO - Why is this making a copy?
            this.soundList = new ArrayList<SoundData>();
            this.soundList.addAll(soundList);
        }

        private class ViewHolder {
            //TextView code;
            CheckBox name;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder holder = null;

            if (convertView == null) {
                LayoutInflater vi = (LayoutInflater)getSystemService( Context.LAYOUT_INFLATER_SERVICE);
                convertView = vi.inflate(R.layout.sound_listitem, null);

                holder = new ViewHolder();
                //holder.code = (TextView) convertView.findViewById(R.id.name);
                holder.name = (CheckBox) convertView.findViewById(R.id.checkBox1);
                convertView.setTag(holder);

                holder.name.setOnClickListener( new View.OnClickListener() {
                    public void onClick(View v) {
                        // Foo
                    }
                });
            }
            else {
                holder = (ViewHolder) convertView.getTag();
            }

            SoundData sound = soundList.get(position);
            //holder.code.setText(" (" +  sound.getCode() + ")");
            //holder.code.setText(" (" +  "code" + ")");
            holder.name.setText(sound.mName);
            // TB TODO - Is selected
            //holder.name.setChecked(sound.isSelected());
            holder.name.setTag(sound);

            return convertView;

        }

    }

}
