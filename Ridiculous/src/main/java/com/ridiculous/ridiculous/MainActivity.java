package com.ridiculous.ridiculous;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.prefs.Preferences;

public class MainActivity extends ActionBarActivity {

    SeekBar mSoundsPerDaySeekbar;
    TextView mSoundsPerDayText;
    SharedPreferences mPrefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mPrefs = PreferenceManager.getDefaultSharedPreferences(this);

        mSoundsPerDayText = (TextView) findViewById( R.id.soundsPerDayText );
        mSoundsPerDaySeekbar = (SeekBar) findViewById( R.id.soundsPerDaySeekBar );

        // TB TODO - Maybe sounds per day is dumb? Instead, it should be approximate frequency? 180 mins, etc?

        int soundsPerDay = mPrefs.getInt( Prefs.SOUNDS_PER_DAY, Prefs.SOUNDS_PER_DAY_DEFAULT );
        mSoundsPerDayText.setText( String.valueOf(soundsPerDay) );
        // -1 because progress of 0 is still going to play 1 sound per day
        mSoundsPerDaySeekbar.setProgress( soundsPerDay - 1 );

        mSoundsPerDaySeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                // Default progress is from 0 to 100
                int numSounds = progress + 1;
                mSoundsPerDayText.setText(String.valueOf(numSounds));

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                int numSounds = seekBar.getProgress() + 1;

                SharedPreferences.Editor editor = mPrefs.edit();
                editor.putInt(Prefs.SOUNDS_PER_DAY, numSounds);
                editor.commit();

            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        //mNetUpdateTask = new NetUpdateTask(this);
        //mNetUpdateTask.execute( Long.valueOf(0) );

        // Start the service to run forever
        Intent checkService = new Intent(this, CheckService.class);
        startService(checkService);
    }

    public void onClickConfigureSounds( View v ) {
        Intent soundList = new Intent(this, SoundsActivity.class);
        startActivity(soundList);
    }

}
