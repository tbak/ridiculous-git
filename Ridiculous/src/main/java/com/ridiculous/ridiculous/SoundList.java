package com.ridiculous.ridiculous;

import java.util.ArrayList;

/**
 * Created by Tom on 6/20/14.
 */
public class SoundList {
    ArrayList<SoundData> mSoundList;

    SoundList() {
        mSoundList = new ArrayList<SoundData>();
        mSoundList.add( new SoundData("Scream", R.raw.wilhelm_scream) );
        mSoundList.add( new SoundData("Fart", R.raw.fart) );
        mSoundList.add( new SoundData("Doh", R.raw.doh) );
    }

    ArrayList<SoundData> getArrayList() {
        return mSoundList;
    }

}
