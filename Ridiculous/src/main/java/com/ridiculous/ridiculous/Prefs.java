package com.ridiculous.ridiculous;

/**
 * Created by Tom on 6/21/14.
 */
public class Prefs {
    public static final String SOUNDS_PER_DAY = "sounds_per_day";
    public static final int SOUNDS_PER_DAY_DEFAULT = 10;
}
