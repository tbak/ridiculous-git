package com.ridiculous.ridiculous;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.BatteryManager;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.content.Intent;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Tom on 6/20/14.
 */
public class CheckService extends Service {

    //SoundPool mSoundPool;
    int mSoundNotify;
    Handler mHandler;
    SoundList mSoundList;
    MediaPlayer mMediaPlayer;
    Intent mBatteryStatus;
    boolean mBatteryIsCharging;

    public class LocalBinder extends Binder {
        CheckService getService() {
            return CheckService.this;
        }
    }


    // This is the object that receives interactions from clients.  See
    // RemoteService for a more complete example.
    private final IBinder mBinder = new LocalBinder();

    @Override
    public IBinder onBind( Intent intent )
    {
        //return null;
        return mBinder;
    }

    BroadcastReceiver mBatteryInfoReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            mBatteryIsCharging = intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1) > 0;
        }
    };

    @Override
    public void onCreate()
    {
        //mSoundPool = new SoundPool( 7, AudioManager.STREAM_MUSIC, 0);
        mHandler = new Handler();
        mSoundList = new SoundList();
        //mMediaPlayer = new MediaPlayer();
        //mMediaPlayer = MediaPlayer.create(this, )
        mMediaPlayer = null;

        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        mBatteryStatus = registerReceiver(null, ifilter);


        Intent batteryIntent = this.registerReceiver(mBatteryInfoReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        mBatteryIsCharging = batteryIntent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1) > 0;
    }

    @Override
    public void onDestroy()
    {
        //instance = null;
        unregisterReceiver(mBatteryInfoReceiver);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Toast.makeText(this, "Starting CheckService!", Toast.LENGTH_SHORT).show();

        /*
        if( mTimeUpdateRunnable != null ) {
            mHandler.removeCallbacks(mTimeUpdateRunnable);
        }
        */

        // If we get killed, after returning from here, restart
        timeUpdate();
        return START_STICKY;
    }

    boolean isBatteryCharging() {
        // For some reason status is always 2, even when not charging???  So use plugged instead
        // int status = mBatteryStatus.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
        // return (status == BatteryManager.BATTERY_STATUS_CHARGING || status == BatteryManager.BATTERY_STATUS_FULL);
        // int plugged = mBatteryStatus.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
        // return plugged > 0;

        return mBatteryIsCharging;
    }

    void playSound() {

        AudioManager audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
        float actualVolume = (float) audioManager .getStreamVolume(AudioManager.STREAM_MUSIC);
        float maxVolume = (float) audioManager .getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        float volume = actualVolume / maxVolume;

        // Max volume always???
        // volume = 1;

        // TB TODO - Get correct sound
        // mSoundNotify = mSoundPool.load(this, R.raw.slot_machine_bet_10, 1);
        //int soundID = 42;

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);

        /*
        if( sharedPref.getBoolean(MainActivity.PLAY_LONG_PREFERENCE,true) ) {
            soundID = mSoundNotifyLong;
        }
        if( isBad ) {
            soundID = mSoundNotifyBad;
        }
        int timeUpdateDelay = sharedPref.getInt( MainActivity.TIME_UPDATE_DELAY_PREFERENCE, 60);
        */

        // TB TODO - Vibrate as well?
        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        v.vibrate(800);

        int resource = getNextSoundResource();

        // TB TODO - Unload the sound?
        /*
        int s = mSoundPool.load( this, resource, 1 );

        mSoundPool.play(s, volume, volume, 1, 0, 1f);
        */


        // TB TODO - Is this best?
        if( mMediaPlayer != null ) {
            mMediaPlayer.stop();
            mMediaPlayer.release();
        }
        mMediaPlayer = MediaPlayer.create(this, resource);
        mMediaPlayer.start();

        /*
        AssetFileDescriptor afd = getResources().openRawResourceFd(resource);
        try {
            mMediaPlayer.setDataSource( afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength() );
            mMediaPlayer.prepare();
            mMediaPlayer.start();
            afd.close();
        }
        catch( Exception e ) {
            Toast.makeText(this, "Error loading sound", Toast.LENGTH_SHORT).show();
        }
        */




        Toast.makeText(this, "I just played a sound!", Toast.LENGTH_SHORT).show();
    }

    int getNextSoundResource() {
        // TB TODO - Only choose from enabled sounds
        ArrayList<SoundData> list = mSoundList.getArrayList();
        Random r = new Random();
        int choice = r.nextInt(list.size());
        return list.get(choice).mResource;
    }

    Runnable mTimeUpdateRunnable = new Runnable() {
        public void run() {
            timeUpdate();
        }
    };

    void timeUpdate() {

        // TB TODO - Logic to determine if a sound should be played

        if( !isBatteryCharging() ) {

            playSound();

        }

        // Convert from seconds to ms
        int timeUpdateDelay = 5;
        mHandler.postDelayed(mTimeUpdateRunnable, timeUpdateDelay * 1000 );
    }

}
