package com.ridiculous.ridiculous;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class BootUpReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Toast.makeText(context, "Ridiculous Boot up receiver!", Toast.LENGTH_SHORT).show();
        Intent checkService = new Intent(context, CheckService.class);
        context.startService(checkService);
    }

}

