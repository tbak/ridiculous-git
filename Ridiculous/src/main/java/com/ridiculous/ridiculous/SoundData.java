package com.ridiculous.ridiculous;

/**
 * Created by Tom on 6/20/14.
 */
public class SoundData {
    public int mID;
    public int mGroup;
    public int mResource;
    public String mName;

    SoundData( String name, int resource ) {
        mName = name;
        mResource = resource;
    }
}


